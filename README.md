# ExoTOM_thesis_files

Mainly jupyter notebooks for creation of plots and investigation for master's thesis developing ExoTOM.

These notebooks need to be copied to an ExoTOM repo and run with `./manage.py shell_plus --notebook`
